#!/usr/bin/python3
"""Prompt Theme.

Description
-----------

This scripts takes a theme file in JSON format and prints a PS1-formatted
string to stdout.

Liscense
--------
Promt-Theme is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Promt-Theme is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Promt-Theme.  If not, see <https://www.gnu.org/licenses/>.

References
----------
- Chris Titus Tech
  - raw.githubusercontent.com/ChrisTitusTech/scripts/master/fancy-bash-promt.sh
- Andres Gongora
  - https://github.com/andresgongora/bash-tools
- WOLFMAN'S color bash prompt
  - https://wiki.chakralinux.org/index.php?title=Color_Bash_Prompt#Wolfman.27s
- https://gist.github.com/MicahElliott/719710
- https://jonasjacek.github.io/colors/
"""
import argparse
import copy
import json
import logging
import os
import pathlib
import sys
from typing import List, Optional


# COLOR_4BIT_D (set at bottom)
# COLOR_8BIT_D (set at bottom)
DEFAULT_COLOR = -1
ENCODING = "utf-8"
INSTALL_PATH = pathlib.Path(__file__).parent.absolute()

############
# General
############


def main(argv: Optional[List[str]] = None):
    """Program entry point. Loads the theme file prints the PS1 to stdout.

    Parameters
    ----------
    argv : list
        The program arguments

    Notes
    -----
    Prints the new prompt to stdout
    """
    options = process_options(argv)
    theme = options["theme"]
    theme = validate_theme(theme)
    prompt_str = get_prompt_str(theme)
    print_prompt(prompt_str)
    sys.exit(0)


def process_options(argv: Optional[List[str]]) -> dict:
    """Parse the program parameters and load results into a dictionary.

    Parameters
    ----------
    argv : list
        The program arguments

    Returns
    -------
    dictionary
        The JSON theme
    """
    parser = argparse.ArgumentParser(
        description="This scripts takes a theme file in JSON format and prints a "
        "PS1-formatted string to stdout."
    )

    parser.add_argument(
        "--theme", "-t", type=str, required=True, help="The theme in JSON format"
    )
    parser.add_argument(
        "--log-level",
        "-l",
        dest="level",
        type=str,
        default="info",
        help="The log level",
    )

    args = parser.parse_args(argv)

    level = args.level.upper()
    if level not in ["INFO", "DEBUG"]:
        error_exit("Invalid log level provided.")

    logging.basicConfig(level=getattr(logging, level))

    theme_file = pathlib.Path(args.theme)

    if not theme_file.is_file():
        theme_file = INSTALL_PATH / "themes" / args.theme
        if not theme_file.is_file():
            error_exit("Could not find theme file")

    theme = None

    try:
        with theme_file.open("r", encoding=ENCODING) as json_file:
            theme = json.load(json_file)
    except IOError as ex:
        logging.error("There was an error finding/reading the file: %s", theme_file)
        logging.debug("Error details", exc_info=ex)
    except json.decoder.JSONDecodeError as ex:
        logging.error("There was an error parsing the JSON file: %s", theme_file)
        logging.debug("Error details", exc_info=ex)

    if theme is None:
        sys.exit(1)
    options = {"theme": theme}

    return options


def error_exit(message: str):
    """Print the given error message and exit the script with an error code.

    Parameters
    ----------
    message : str
        The message to print
    """
    logging.error(message)
    sys.exit(1)


############
# Theme
############


def validate_theme(theme: List[dict]) -> List[dict]:
    """Validate the theme components and fill in any gaps with defaults.

    Parameters
    ----------
    theme : list
        The theme components

    Returns
    -------
    list
        The updated theme components
    """
    if len(theme) == 0:
        error_exit("JSON outer element must be an array of one or more elements")

    new_theme = copy.deepcopy(theme)
    for cp_idx, component in enumerate(new_theme):
        new_theme[cp_idx] = validate_component(component)
    return new_theme


def validate_component(component: dict) -> dict:
    """Validate an individual theme component and default any gaps.

    Parameters
    ----------
    component : dict
        The theme component

    Returns
    -------
    dict
        The updated theme component
    """
    if not isinstance(component, dict):
        error_exit("One of the elements is not a valid JSON object")
    new_component = copy.deepcopy(component)
    if "text" not in new_component:
        new_component["text"] = None

    if "env" not in new_component:
        new_component["env"] = None

    if new_component["env"] is not None and new_component["text"] is None:
        new_component["type"] = "env"

    if "type" not in new_component or new_component["type"] is None:
        new_component["type"] = "text"

    if (
        new_component["type"].lower() != "text"
        and new_component["type"].lower() != "env"
        and new_component["type"].lower() != "reset"
    ):
        error_exit("Invalid type: " + new_component["type"])

    if "color" not in new_component:
        new_component["color"] = None

    if new_component["color"] is not None:
        if "depth" not in new_component["color"]:
            new_component["color"]["depth"] = 4
        if "fg" not in new_component["color"]:
            new_component["color"]["fg"] = None
        # validate_color(new_component["color"]["fg"])
        if "bg" not in new_component["color"]:
            new_component["color"]["bg"] = None
        # validate_color(new_component["color"]["bg"])
        if "effect" not in new_component["color"]:
            new_component["color"]["effect"] = None
        # TODO: validate effect
        if (
            new_component["color"]["fg"] is None
            and new_component["color"]["bg"] is None
            and new_component["color"]["effect"] is None
        ):
            new_component["color"] = None

    return new_component

    # def validate_color(color: str) -> bool:
    #     """Validate a color value.
    #
    #     Parameters
    #     ----------
    #     color : str
    #         The color
    #
    #     Returns
    #     -------
    #     bool
    #         Indicator as to the color's validity
    #
    #     Notes
    #     -----
    #     Needs work, so disabled for now
    #     """
    #
    #     return True
    #     #if color.isdigit()
    #     #    if color < 0 or color > 255:
    #     #        error_exit(
    #     #            'Color outside expected range of 0 to 255 (%s)' % color)
    #     #todo: names


def get_prompt_str(theme: List[dict]) -> str:
    """Get a prompt string from the theme.

    Parameters
    ----------
    theme : list
        The theme components

    Returns
    -------
    str
        The prompt string
    """
    prompt_str = ""
    for component in theme:
        text = get_component_str(component)
        if text is not None:
            prompt_str += text

    return prompt_str


def get_component_str(component: dict) -> str:
    """Get a prompt string from the theme component.

    Parameters
    ----------
    component : dict
        The theme component

    Returns
    -------
    str
        The prompt string
    """
    cmp_type = component["type"].lower()
    text = component["text"]
    has_color = component["color"] is not None
    if cmp_type == "reset":
        text = format_reset_text()
        has_color = False
    elif cmp_type == "env":
        text = os.environ[component["env"]]

    if text is not None and has_color:
        text = f'{format_color(component["color"])}{text}'

    return text


############
# Color
############


def get_color_code_from_name_depth(color_name: str, color_depth: int) -> int:
    """Return the color escape code from the color name and bit depth.

    Parameters
    ----------
    color_name : str
        The color name

    color_depth : int
        The color bit depth (4 or 8)

    Returns
    -------
    int
        The color escape code
    """
    if color_depth == 8:
        return COLOR_8BIT_D["name_map"][color_name]
    else:
        return COLOR_4BIT_D["name_map"][color_name]


def get_effect_code_from_name(effect_name: str) -> int:
    """Return the effect escape code from the given name.

    Parameters
    ----------
    effect_name : str
        The effect name

    Returns
    -------
    int
        The effect escape code
    """
    if effect_name is None:
        return None

    effect_name_uc = effect_name.upper()
    code = None

    if effect_name_uc == "NONE":
        code = 0
    elif effect_name_uc == "BOLD":
        code = 1
    elif effect_name_uc == "DIM":
        code = 2
    elif effect_name_uc == "UNDERLINE":
        code = 4
    elif effect_name_uc == "BLINK":
        code = 5
    elif effect_name_uc == "INVERT":
        code = 7
    elif effect_name_uc == "HIDDEN":
        code = 8
    else:
        error_exit("Unrecognized effect name")

    return code


def get_font_format(fg_color: str, bg_color: str, effect: int) -> str:
    """Return the prompt format string for the given colors and effect.

    Parameters
    ----------
    fg_color : str
        The foreground color format

    bg_color : str
        The background color format

    effect : int
        The effect escape code

    Returns
    -------
    str
        The combined prompt format
    """
    format_str = "\\[\\033["
    formats = []
    if effect is not None:
        formats.append(str(effect))
    if fg_color is not None:
        formats.append(str(fg_color))
    if bg_color is not None:
        formats.append(str(bg_color))

    format_str += ";".join(formats) + "m\\]"

    return format_str


def format_color(color: dict) -> str:
    """Return the prompt format string for the component color definition.

    Parameters
    ----------
    color : dict
        The component color definition

    Returns
    -------
    str
        The combined prompt format
    """
    fg_color = get_color_code(color["fg"], "FG", color["depth"])
    bg_color = get_color_code(color["bg"], "BG", color["depth"])
    effect = get_effect_code_from_name(color["effect"])

    return get_font_format(fg_color, bg_color, effect)


def get_color_code(color: str, cmp_type: str, depth: int) -> str:
    """Return the color format for the corresponding color definiton.

    Parameters
    ----------
    color : str
        The color code or name

    cmp_type : str
        FG or BG for foreground or background respectively

    depth : int
        The color bit depth

    Returns
    -------
    str
        The color's prompt format string
    """
    code = None
    if color is not None:
        color_raw = color.lower()
        if len(color_raw) == 7 and color_raw[:1] == "#":
            code = get_color_code_from_number_type_depth(
                get_best_color_from_hex_depth(color_raw[1:], depth), cmp_type, depth
            )
        elif color_raw[:3].lower() == "rgb":
            code = get_color_code_from_number_type_depth(
                get_best_color_from_rgb_depth(color_raw[4:-1], depth), cmp_type, depth
            )
        elif color_raw.isdigit():
            code = get_color_code_from_number_type_depth(color_raw, cmp_type, depth)
        else:
            code = get_color_code_from_number_type_depth(
                get_color_code_from_name_depth(color_raw, depth), cmp_type, depth
            )
    return code


def get_best_color_from_hex_depth(hex_cd: str, depth: int) -> int:
    """Return the best color code from the given hex RGB value.

    Parameters
    ----------
    hex_cd : str
        The color code in 6-digit hex format

    depth : int
        The color bit depth (only 8 is supported)

    Returns
    -------
    int
        The color escape code
    """
    if depth != 8:
        error_exit("Hex conversion only supported on 8-bit color depth")

    c_r = hex_cd[:2].lower()
    c_g = hex_cd[2:-2].lower()
    c_b = hex_cd[-2:].lower()
    if c_r == c_g and c_g == c_b:
        return COLOR_8BIT_D["hex_map"][
            (
                "".join(
                    [
                        "#",
                        COLOR_8BIT_D["best_grey_hex"][c_r],
                        COLOR_8BIT_D["best_grey_hex"][c_g],
                        COLOR_8BIT_D["best_grey_hex"][c_b],
                    ]
                )
            )
        ]
    else:
        return COLOR_8BIT_D["hex_map"][
            (
                "".join(
                    [
                        "#",
                        COLOR_8BIT_D["best_hex"][c_r],
                        COLOR_8BIT_D["best_hex"][c_g],
                        COLOR_8BIT_D["best_hex"][c_b],
                    ]
                )
            )
        ]


def get_best_color_from_rgb_depth(rgb_cd: str, depth: int) -> int:
    """Return the best color code from the given rgb value.

    Parameters
    ----------
    hex_cd : str
        The color code in rgb(r,g,b) format

    depth : int
        The color bit depth (only 8 is supported)

    Returns
    -------
    int
        The color escape code
    """
    if depth != 8:
        error_exit("RGB conversion only supported on 8-bit color depth")

    rgb = rgb_cd.split(",")
    c_r = rgb[0]
    c_g = rgb[1]
    c_b = rgb[2]
    if c_r == c_g and c_g == c_b:
        return COLOR_8BIT_D["rgb_map"][
            (
                "".join(
                    [
                        "rgb(",
                        COLOR_8BIT_D["best_grey_rgb"][int(c_r)],
                        ",",
                        COLOR_8BIT_D["best_grey_rgb"][int(c_g)],
                        ",",
                        COLOR_8BIT_D["best_grey_rgb"][int(c_b)],
                        ")",
                    ]
                )
            )
        ]
    else:
        return COLOR_8BIT_D["rgb_map"][
            (
                "".join(
                    [
                        "rgb(",
                        COLOR_8BIT_D["best_rgb"][int(c_r)],
                        ",",
                        COLOR_8BIT_D["best_rgb"][int(c_g)],
                        ",",
                        COLOR_8BIT_D["best_rgb"][int(c_b)],
                        ")",
                    ]
                )
            )
        ]


def get_color_code_from_number_type_depth(code: int, cmp_type: str, depth: int) -> str:
    """Return the best color code from the given code.

    Parameters
    ----------
    code : int
        The color code (0-255)

    cmp_type : str
        FG or BG for foreground or background respectively

    depth : int
        The color bit depth

    Returns
    -------
    int
        The color escape code
    """
    type_cd = 38
    offset = 30
    if cmp_type == "BG":
        type_cd = 48
        offset = 40
    int_code = int(code)
    if int_code == DEFAULT_COLOR:
        return str(9 + offset)
    elif depth == 8:
        return f"{type_cd};5;{code}"
    else:
        return int_code + offset


def get_color_8_bit_lookup_dict() -> dict:
    """Return an 8-bit color code lookup dictionary.

    Returns
    -------
    dict
        The 8-bit color code lookup dictionary
    """
    search_colors = [
        {"color_id": 0, "name": "Black", "rgb": {"r": 0, "g": 0, "b": 0}},
        {"color_id": 1, "name": "Maroon", "rgb": {"r": 128, "g": 0, "b": 0}},
        {"color_id": 2, "name": "Green", "rgb": {"r": 0, "g": 128, "b": 0}},
        {"color_id": 3, "name": "Olive", "rgb": {"r": 128, "g": 128, "b": 0}},
        {"color_id": 4, "name": "Navy", "rgb": {"r": 0, "g": 0, "b": 128}},
        {"color_id": 5, "name": "Purple", "rgb": {"r": 128, "g": 0, "b": 128}},
        {"color_id": 6, "name": "Teal", "rgb": {"r": 0, "g": 128, "b": 128}},
        {"color_id": 8, "name": "Grey", "rgb": {"r": 128, "g": 128, "b": 128}},
        {"color_id": 9, "name": "Red", "rgb": {"r": 255, "g": 0, "b": 0}},
        {"color_id": 10, "name": "Lime", "rgb": {"r": 0, "g": 255, "b": 0}},
        {"color_id": 11, "name": "Yellow", "rgb": {"r": 255, "g": 255, "b": 0}},
        {"color_id": 12, "name": "Blue", "rgb": {"r": 0, "g": 0, "b": 255}},
        {"color_id": 13, "name": "Fuchsia", "rgb": {"r": 255, "g": 0, "b": 255}},
        {"color_id": 14, "name": "Aqua", "rgb": {"r": 0, "g": 255, "b": 255}},
        {"color_id": 15, "name": "White", "rgb": {"r": 255, "g": 255, "b": 255}},
        {"color_id": 16, "name": "Grey0", "rgb": {"r": 0, "g": 0, "b": 0}},
        {"color_id": 17, "name": "NavyBlue", "rgb": {"r": 0, "g": 0, "b": 95}},
        {"color_id": 18, "name": "DarkBlue", "rgb": {"r": 0, "g": 0, "b": 135}},
        {"color_id": 19, "name": "Blue3", "rgb": {"r": 0, "g": 0, "b": 175}},
        {"color_id": 20, "name": "Blue3", "rgb": {"r": 0, "g": 0, "b": 215}},
        {"color_id": 21, "name": "Blue1", "rgb": {"r": 0, "g": 0, "b": 255}},
        {"color_id": 22, "name": "DarkGreen", "rgb": {"r": 0, "g": 95, "b": 0}},
        {"color_id": 23, "name": "DeepSkyBlue4", "rgb": {"r": 0, "g": 95, "b": 95}},
        {"color_id": 24, "name": "DeepSkyBlue4", "rgb": {"r": 0, "g": 95, "b": 135}},
        {"color_id": 25, "name": "DeepSkyBlue4", "rgb": {"r": 0, "g": 95, "b": 175}},
        {"color_id": 26, "name": "DodgerBlue3", "rgb": {"r": 0, "g": 95, "b": 215}},
        {"color_id": 27, "name": "DodgerBlue2", "rgb": {"r": 0, "g": 95, "b": 255}},
        {"color_id": 28, "name": "Green4", "rgb": {"r": 0, "g": 135, "b": 0}},
        {"color_id": 29, "name": "SpringGreen4", "rgb": {"r": 0, "g": 135, "b": 95}},
        {"color_id": 30, "name": "Turquoise4", "rgb": {"r": 0, "g": 135, "b": 135}},
        {"color_id": 31, "name": "DeepSkyBlue3", "rgb": {"r": 0, "g": 135, "b": 175}},
        {"color_id": 32, "name": "DeepSkyBlue3", "rgb": {"r": 0, "g": 135, "b": 215}},
        {"color_id": 33, "name": "DodgerBlue1", "rgb": {"r": 0, "g": 135, "b": 255}},
        {"color_id": 34, "name": "Green3", "rgb": {"r": 0, "g": 175, "b": 0}},
        {"color_id": 35, "name": "SpringGreen3", "rgb": {"r": 0, "g": 175, "b": 95}},
        {"color_id": 36, "name": "DarkCyan", "rgb": {"r": 0, "g": 175, "b": 135}},
        {"color_id": 37, "name": "LightSeaGreen", "rgb": {"r": 0, "g": 175, "b": 175}},
        {"color_id": 38, "name": "DeepSkyBlue2", "rgb": {"r": 0, "g": 175, "b": 215}},
        {"color_id": 39, "name": "DeepSkyBlue1", "rgb": {"r": 0, "g": 175, "b": 255}},
        {"color_id": 40, "name": "Green3", "rgb": {"r": 0, "g": 215, "b": 0}},
        {"color_id": 41, "name": "SpringGreen3", "rgb": {"r": 0, "g": 215, "b": 95}},
        {"color_id": 42, "name": "SpringGreen2", "rgb": {"r": 0, "g": 215, "b": 135}},
        {"color_id": 43, "name": "Cyan3", "rgb": {"r": 0, "g": 215, "b": 175}},
        {"color_id": 44, "name": "DarkTurquoise", "rgb": {"r": 0, "g": 215, "b": 215}},
        {"color_id": 45, "name": "Turquoise2", "rgb": {"r": 0, "g": 215, "b": 255}},
        {"color_id": 46, "name": "Green1", "rgb": {"r": 0, "g": 255, "b": 0}},
        {"color_id": 47, "name": "SpringGreen2", "rgb": {"r": 0, "g": 255, "b": 95}},
        {"color_id": 48, "name": "SpringGreen1", "rgb": {"r": 0, "g": 255, "b": 135}},
        {
            "color_id": 49,
            "name": "MediumSpringGreen",
            "rgb": {"r": 0, "g": 255, "b": 175},
        },
        {"color_id": 50, "name": "Cyan2", "rgb": {"r": 0, "g": 255, "b": 215}},
        {"color_id": 51, "name": "Cyan1", "rgb": {"r": 0, "g": 255, "b": 255}},
        {"color_id": 52, "name": "DarkRed", "rgb": {"r": 95, "g": 0, "b": 0}},
        {"color_id": 53, "name": "DeepPink4", "rgb": {"r": 95, "g": 0, "b": 95}},
        {"color_id": 54, "name": "Purple4", "rgb": {"r": 95, "g": 0, "b": 135}},
        {"color_id": 55, "name": "Purple4", "rgb": {"r": 95, "g": 0, "b": 175}},
        {"color_id": 56, "name": "Purple3", "rgb": {"r": 95, "g": 0, "b": 215}},
        {"color_id": 57, "name": "BlueViolet", "rgb": {"r": 95, "g": 0, "b": 255}},
        {"color_id": 58, "name": "Orange4", "rgb": {"r": 95, "g": 95, "b": 0}},
        {"color_id": 59, "name": "Grey37", "rgb": {"r": 95, "g": 95, "b": 95}},
        {"color_id": 60, "name": "MediumPurple4", "rgb": {"r": 95, "g": 95, "b": 135}},
        {"color_id": 61, "name": "SlateBlue3", "rgb": {"r": 95, "g": 95, "b": 175}},
        {"color_id": 62, "name": "SlateBlue3", "rgb": {"r": 95, "g": 95, "b": 215}},
        {"color_id": 63, "name": "RoyalBlue1", "rgb": {"r": 95, "g": 95, "b": 255}},
        {"color_id": 64, "name": "Chartreuse4", "rgb": {"r": 95, "g": 135, "b": 0}},
        {"color_id": 65, "name": "DarkSeaGreen4", "rgb": {"r": 95, "g": 135, "b": 95}},
        {
            "color_id": 66,
            "name": "PaleTurquoise4",
            "rgb": {"r": 95, "g": 135, "b": 135},
        },
        {"color_id": 67, "name": "SteelBlue", "rgb": {"r": 95, "g": 135, "b": 175}},
        {"color_id": 68, "name": "SteelBlue3", "rgb": {"r": 95, "g": 135, "b": 215}},
        {
            "color_id": 69,
            "name": "CornflowerBlue",
            "rgb": {"r": 95, "g": 135, "b": 255},
        },
        {"color_id": 70, "name": "Chartreuse3", "rgb": {"r": 95, "g": 175, "b": 0}},
        {"color_id": 71, "name": "DarkSeaGreen4", "rgb": {"r": 95, "g": 175, "b": 95}},
        {"color_id": 72, "name": "CadetBlue", "rgb": {"r": 95, "g": 175, "b": 135}},
        {"color_id": 73, "name": "CadetBlue", "rgb": {"r": 95, "g": 175, "b": 175}},
        {"color_id": 74, "name": "SkyBlue3", "rgb": {"r": 95, "g": 175, "b": 215}},
        {"color_id": 75, "name": "SteelBlue1", "rgb": {"r": 95, "g": 175, "b": 255}},
        {"color_id": 76, "name": "Chartreuse3", "rgb": {"r": 95, "g": 215, "b": 0}},
        {"color_id": 77, "name": "PaleGreen3", "rgb": {"r": 95, "g": 215, "b": 95}},
        {"color_id": 78, "name": "SeaGreen3", "rgb": {"r": 95, "g": 215, "b": 135}},
        {"color_id": 79, "name": "Aquamarine3", "rgb": {"r": 95, "g": 215, "b": 175}},
        {
            "color_id": 80,
            "name": "MediumTurquoise",
            "rgb": {"r": 95, "g": 215, "b": 215},
        },
        {"color_id": 81, "name": "SteelBlue1", "rgb": {"r": 95, "g": 215, "b": 255}},
        {"color_id": 82, "name": "Chartreuse2", "rgb": {"r": 95, "g": 255, "b": 0}},
        {"color_id": 83, "name": "SeaGreen2", "rgb": {"r": 95, "g": 255, "b": 95}},
        {"color_id": 84, "name": "SeaGreen1", "rgb": {"r": 95, "g": 255, "b": 135}},
        {"color_id": 85, "name": "SeaGreen1", "rgb": {"r": 95, "g": 255, "b": 175}},
        {"color_id": 86, "name": "Aquamarine1", "rgb": {"r": 95, "g": 255, "b": 215}},
        {
            "color_id": 87,
            "name": "DarkSlateGray2",
            "rgb": {"r": 95, "g": 255, "b": 255},
        },
        {"color_id": 88, "name": "DarkRed", "rgb": {"r": 135, "g": 0, "b": 0}},
        {"color_id": 89, "name": "DeepPink4", "rgb": {"r": 135, "g": 0, "b": 95}},
        {"color_id": 90, "name": "DarkMagenta", "rgb": {"r": 135, "g": 0, "b": 135}},
        {"color_id": 91, "name": "DarkMagenta", "rgb": {"r": 135, "g": 0, "b": 175}},
        {"color_id": 92, "name": "DarkViolet", "rgb": {"r": 135, "g": 0, "b": 215}},
        {"color_id": 93, "name": "Purple", "rgb": {"r": 135, "g": 0, "b": 255}},
        {"color_id": 94, "name": "Orange4", "rgb": {"r": 135, "g": 95, "b": 0}},
        {"color_id": 95, "name": "LightPink4", "rgb": {"r": 135, "g": 95, "b": 95}},
        {"color_id": 96, "name": "Plum4", "rgb": {"r": 135, "g": 95, "b": 135}},
        {"color_id": 97, "name": "MediumPurple3", "rgb": {"r": 135, "g": 95, "b": 175}},
        {"color_id": 98, "name": "MediumPurple3", "rgb": {"r": 135, "g": 95, "b": 215}},
        {"color_id": 99, "name": "SlateBlue1", "rgb": {"r": 135, "g": 95, "b": 255}},
        {"color_id": 100, "name": "Yellow4", "rgb": {"r": 135, "g": 135, "b": 0}},
        {"color_id": 101, "name": "Wheat4", "rgb": {"r": 135, "g": 135, "b": 95}},
        {"color_id": 102, "name": "Grey53", "rgb": {"r": 135, "g": 135, "b": 135}},
        {
            "color_id": 103,
            "name": "LightSlateGrey",
            "rgb": {"r": 135, "g": 135, "b": 175},
        },
        {
            "color_id": 104,
            "name": "MediumPurple",
            "rgb": {"r": 135, "g": 135, "b": 215},
        },
        {
            "color_id": 105,
            "name": "LightSlateBlue",
            "rgb": {"r": 135, "g": 135, "b": 255},
        },
        {"color_id": 106, "name": "Yellow4", "rgb": {"r": 135, "g": 175, "b": 0}},
        {
            "color_id": 107,
            "name": "DarkOliveGreen3",
            "rgb": {"r": 135, "g": 175, "b": 95},
        },
        {
            "color_id": 108,
            "name": "DarkSeaGreen",
            "rgb": {"r": 135, "g": 175, "b": 135},
        },
        {
            "color_id": 109,
            "name": "LightSkyBlue3",
            "rgb": {"r": 135, "g": 175, "b": 175},
        },
        {
            "color_id": 110,
            "name": "LightSkyBlue3",
            "rgb": {"r": 135, "g": 175, "b": 215},
        },
        {"color_id": 111, "name": "SkyBlue2", "rgb": {"r": 135, "g": 175, "b": 255}},
        {"color_id": 112, "name": "Chartreuse2", "rgb": {"r": 135, "g": 215, "b": 0}},
        {
            "color_id": 113,
            "name": "DarkOliveGreen3",
            "rgb": {"r": 135, "g": 215, "b": 95},
        },
        {"color_id": 114, "name": "PaleGreen3", "rgb": {"r": 135, "g": 215, "b": 135}},
        {
            "color_id": 115,
            "name": "DarkSeaGreen3",
            "rgb": {"r": 135, "g": 215, "b": 175},
        },
        {
            "color_id": 116,
            "name": "DarkSlateGray3",
            "rgb": {"r": 135, "g": 215, "b": 215},
        },
        {"color_id": 117, "name": "SkyBlue1", "rgb": {"r": 135, "g": 215, "b": 255}},
        {"color_id": 118, "name": "Chartreuse1", "rgb": {"r": 135, "g": 255, "b": 0}},
        {"color_id": 119, "name": "LightGreen", "rgb": {"r": 135, "g": 255, "b": 95}},
        {"color_id": 120, "name": "LightGreen", "rgb": {"r": 135, "g": 255, "b": 135}},
        {"color_id": 121, "name": "PaleGreen1", "rgb": {"r": 135, "g": 255, "b": 175}},
        {"color_id": 122, "name": "Aquamarine1", "rgb": {"r": 135, "g": 255, "b": 215}},
        {
            "color_id": 123,
            "name": "DarkSlateGray1",
            "rgb": {"r": 135, "g": 255, "b": 255},
        },
        {"color_id": 124, "name": "Red3", "rgb": {"r": 175, "g": 0, "b": 0}},
        {"color_id": 125, "name": "DeepPink4", "rgb": {"r": 175, "g": 0, "b": 95}},
        {
            "color_id": 126,
            "name": "MediumVioletRed",
            "rgb": {"r": 175, "g": 0, "b": 135},
        },
        {"color_id": 127, "name": "Magenta3", "rgb": {"r": 175, "g": 0, "b": 175}},
        {"color_id": 128, "name": "DarkViolet", "rgb": {"r": 175, "g": 0, "b": 215}},
        {"color_id": 129, "name": "Purple", "rgb": {"r": 175, "g": 0, "b": 255}},
        {"color_id": 130, "name": "DarkOrange3", "rgb": {"r": 175, "g": 95, "b": 0}},
        {"color_id": 131, "name": "IndianRed", "rgb": {"r": 175, "g": 95, "b": 95}},
        {"color_id": 132, "name": "HotPink3", "rgb": {"r": 175, "g": 95, "b": 135}},
        {
            "color_id": 133,
            "name": "MediumOrchid3",
            "rgb": {"r": 175, "g": 95, "b": 175},
        },
        {"color_id": 134, "name": "MediumOrchid", "rgb": {"r": 175, "g": 95, "b": 215}},
        {
            "color_id": 135,
            "name": "MediumPurple2",
            "rgb": {"r": 175, "g": 95, "b": 255},
        },
        {"color_id": 136, "name": "DarkGoldenrod", "rgb": {"r": 175, "g": 135, "b": 0}},
        {"color_id": 137, "name": "LightSalmon3", "rgb": {"r": 175, "g": 135, "b": 95}},
        {"color_id": 138, "name": "RosyBrown", "rgb": {"r": 175, "g": 135, "b": 135}},
        {"color_id": 139, "name": "Grey63", "rgb": {"r": 175, "g": 135, "b": 175}},
        {
            "color_id": 140,
            "name": "MediumPurple2",
            "rgb": {"r": 175, "g": 135, "b": 215},
        },
        {
            "color_id": 141,
            "name": "MediumPurple1",
            "rgb": {"r": 175, "g": 135, "b": 255},
        },
        {"color_id": 142, "name": "Gold3", "rgb": {"r": 175, "g": 175, "b": 0}},
        {"color_id": 143, "name": "DarkKhaki", "rgb": {"r": 175, "g": 175, "b": 95}},
        {
            "color_id": 144,
            "name": "NavajoWhite3",
            "rgb": {"r": 175, "g": 175, "b": 135},
        },
        {"color_id": 145, "name": "Grey69", "rgb": {"r": 175, "g": 175, "b": 175}},
        {
            "color_id": 146,
            "name": "LightSteelBlue3",
            "rgb": {"r": 175, "g": 175, "b": 215},
        },
        {
            "color_id": 147,
            "name": "LightSteelBlue",
            "rgb": {"r": 175, "g": 175, "b": 255},
        },
        {"color_id": 148, "name": "Yellow3", "rgb": {"r": 175, "g": 215, "b": 0}},
        {
            "color_id": 149,
            "name": "DarkOliveGreen3",
            "rgb": {"r": 175, "g": 215, "b": 95},
        },
        {
            "color_id": 150,
            "name": "DarkSeaGreen3",
            "rgb": {"r": 175, "g": 215, "b": 135},
        },
        {
            "color_id": 151,
            "name": "DarkSeaGreen2",
            "rgb": {"r": 175, "g": 215, "b": 175},
        },
        {"color_id": 152, "name": "LightCyan3", "rgb": {"r": 175, "g": 215, "b": 215}},
        {
            "color_id": 153,
            "name": "LightSkyBlue1",
            "rgb": {"r": 175, "g": 215, "b": 255},
        },
        {"color_id": 154, "name": "GreenYellow", "rgb": {"r": 175, "g": 255, "b": 0}},
        {
            "color_id": 155,
            "name": "DarkOliveGreen2",
            "rgb": {"r": 175, "g": 255, "b": 95},
        },
        {"color_id": 156, "name": "PaleGreen1", "rgb": {"r": 175, "g": 255, "b": 135}},
        {
            "color_id": 157,
            "name": "DarkSeaGreen2",
            "rgb": {"r": 175, "g": 255, "b": 175},
        },
        {
            "color_id": 158,
            "name": "DarkSeaGreen1",
            "rgb": {"r": 175, "g": 255, "b": 215},
        },
        {
            "color_id": 159,
            "name": "PaleTurquoise1",
            "rgb": {"r": 175, "g": 255, "b": 255},
        },
        {"color_id": 160, "name": "Red3", "rgb": {"r": 215, "g": 0, "b": 0}},
        {"color_id": 161, "name": "DeepPink3", "rgb": {"r": 215, "g": 0, "b": 95}},
        {"color_id": 162, "name": "DeepPink3", "rgb": {"r": 215, "g": 0, "b": 135}},
        {"color_id": 163, "name": "Magenta3", "rgb": {"r": 215, "g": 0, "b": 175}},
        {"color_id": 164, "name": "Magenta3", "rgb": {"r": 215, "g": 0, "b": 215}},
        {"color_id": 165, "name": "Magenta2", "rgb": {"r": 215, "g": 0, "b": 255}},
        {"color_id": 166, "name": "DarkOrange3", "rgb": {"r": 215, "g": 95, "b": 0}},
        {"color_id": 167, "name": "IndianRed", "rgb": {"r": 215, "g": 95, "b": 95}},
        {"color_id": 168, "name": "HotPink3", "rgb": {"r": 215, "g": 95, "b": 135}},
        {"color_id": 169, "name": "HotPink2", "rgb": {"r": 215, "g": 95, "b": 175}},
        {"color_id": 170, "name": "Orchid", "rgb": {"r": 215, "g": 95, "b": 215}},
        {
            "color_id": 171,
            "name": "MediumOrchid1",
            "rgb": {"r": 215, "g": 95, "b": 255},
        },
        {"color_id": 172, "name": "Orange3", "rgb": {"r": 215, "g": 135, "b": 0}},
        {"color_id": 173, "name": "LightSalmon3", "rgb": {"r": 215, "g": 135, "b": 95}},
        {"color_id": 174, "name": "LightPink3", "rgb": {"r": 215, "g": 135, "b": 135}},
        {"color_id": 175, "name": "Pink3", "rgb": {"r": 215, "g": 135, "b": 175}},
        {"color_id": 176, "name": "Plum3", "rgb": {"r": 215, "g": 135, "b": 215}},
        {"color_id": 177, "name": "Violet", "rgb": {"r": 215, "g": 135, "b": 255}},
        {"color_id": 178, "name": "Gold3", "rgb": {"r": 215, "g": 175, "b": 0}},
        {
            "color_id": 179,
            "name": "LightGoldenrod3",
            "rgb": {"r": 215, "g": 175, "b": 95},
        },
        {"color_id": 180, "name": "Tan", "rgb": {"r": 215, "g": 175, "b": 135}},
        {"color_id": 181, "name": "MistyRose3", "rgb": {"r": 215, "g": 175, "b": 175}},
        {"color_id": 182, "name": "Thistle3", "rgb": {"r": 215, "g": 175, "b": 215}},
        {"color_id": 183, "name": "Plum2", "rgb": {"r": 215, "g": 175, "b": 255}},
        {"color_id": 184, "name": "Yellow3", "rgb": {"r": 215, "g": 215, "b": 0}},
        {"color_id": 185, "name": "Khaki3", "rgb": {"r": 215, "g": 215, "b": 95}},
        {
            "color_id": 186,
            "name": "LightGoldenrod2",
            "rgb": {"r": 215, "g": 215, "b": 135},
        },
        {
            "color_id": 187,
            "name": "LightYellow3",
            "rgb": {"r": 215, "g": 215, "b": 175},
        },
        {"color_id": 188, "name": "Grey84", "rgb": {"r": 215, "g": 215, "b": 215}},
        {
            "color_id": 189,
            "name": "LightSteelBlue1",
            "rgb": {"r": 215, "g": 215, "b": 255},
        },
        {"color_id": 190, "name": "Yellow2", "rgb": {"r": 215, "g": 255, "b": 0}},
        {
            "color_id": 191,
            "name": "DarkOliveGreen1",
            "rgb": {"r": 215, "g": 255, "b": 95},
        },
        {
            "color_id": 192,
            "name": "DarkOliveGreen1",
            "rgb": {"r": 215, "g": 255, "b": 135},
        },
        {
            "color_id": 193,
            "name": "DarkSeaGreen1",
            "rgb": {"r": 215, "g": 255, "b": 175},
        },
        {"color_id": 194, "name": "Honeydew2", "rgb": {"r": 215, "g": 255, "b": 215}},
        {"color_id": 195, "name": "LightCyan1", "rgb": {"r": 215, "g": 255, "b": 255}},
        {"color_id": 196, "name": "Red1", "rgb": {"r": 255, "g": 0, "b": 0}},
        {"color_id": 197, "name": "DeepPink2", "rgb": {"r": 255, "g": 0, "b": 95}},
        {"color_id": 198, "name": "DeepPink1", "rgb": {"r": 255, "g": 0, "b": 135}},
        {"color_id": 199, "name": "DeepPink1", "rgb": {"r": 255, "g": 0, "b": 175}},
        {"color_id": 200, "name": "Magenta2", "rgb": {"r": 255, "g": 0, "b": 215}},
        {"color_id": 201, "name": "Magenta1", "rgb": {"r": 255, "g": 0, "b": 255}},
        {"color_id": 202, "name": "OrangeRed1", "rgb": {"r": 255, "g": 95, "b": 0}},
        {"color_id": 203, "name": "IndianRed1", "rgb": {"r": 255, "g": 95, "b": 95}},
        {"color_id": 204, "name": "IndianRed1", "rgb": {"r": 255, "g": 95, "b": 135}},
        {"color_id": 205, "name": "HotPink", "rgb": {"r": 255, "g": 95, "b": 175}},
        {"color_id": 206, "name": "HotPink", "rgb": {"r": 255, "g": 95, "b": 215}},
        {
            "color_id": 207,
            "name": "MediumOrchid1",
            "rgb": {"r": 255, "g": 95, "b": 255},
        },
        {"color_id": 208, "name": "DarkOrange", "rgb": {"r": 255, "g": 135, "b": 0}},
        {"color_id": 209, "name": "Salmon1", "rgb": {"r": 255, "g": 135, "b": 95}},
        {"color_id": 210, "name": "LightCoral", "rgb": {"r": 255, "g": 135, "b": 135}},
        {
            "color_id": 211,
            "name": "PaleVioletRed1",
            "rgb": {"r": 255, "g": 135, "b": 175},
        },
        {"color_id": 212, "name": "Orchid2", "rgb": {"r": 255, "g": 135, "b": 215}},
        {"color_id": 213, "name": "Orchid1", "rgb": {"r": 255, "g": 135, "b": 255}},
        {"color_id": 214, "name": "Orange1", "rgb": {"r": 255, "g": 175, "b": 0}},
        {"color_id": 215, "name": "SandyBrown", "rgb": {"r": 255, "g": 175, "b": 95}},
        {
            "color_id": 216,
            "name": "LightSalmon1",
            "rgb": {"r": 255, "g": 175, "b": 135},
        },
        {"color_id": 217, "name": "LightPink1", "rgb": {"r": 255, "g": 175, "b": 175}},
        {"color_id": 218, "name": "Pink1", "rgb": {"r": 255, "g": 175, "b": 215}},
        {"color_id": 219, "name": "Plum1", "rgb": {"r": 255, "g": 175, "b": 255}},
        {"color_id": 220, "name": "Gold1", "rgb": {"r": 255, "g": 215, "b": 0}},
        {
            "color_id": 221,
            "name": "LightGoldenrod2",
            "rgb": {"r": 255, "g": 215, "b": 95},
        },
        {
            "color_id": 222,
            "name": "LightGoldenrod2",
            "rgb": {"r": 255, "g": 215, "b": 135},
        },
        {
            "color_id": 223,
            "name": "NavajoWhite1",
            "rgb": {"r": 255, "g": 215, "b": 175},
        },
        {"color_id": 224, "name": "MistyRose1", "rgb": {"r": 255, "g": 215, "b": 215}},
        {"color_id": 225, "name": "Thistle1", "rgb": {"r": 255, "g": 215, "b": 255}},
        {"color_id": 226, "name": "Yellow1", "rgb": {"r": 255, "g": 255, "b": 0}},
        {
            "color_id": 227,
            "name": "LightGoldenrod1",
            "rgb": {"r": 255, "g": 255, "b": 95},
        },
        {"color_id": 228, "name": "Khaki1", "rgb": {"r": 255, "g": 255, "b": 135}},
        {"color_id": 229, "name": "Wheat1", "rgb": {"r": 255, "g": 255, "b": 175}},
        {"color_id": 230, "name": "Cornsilk1", "rgb": {"r": 255, "g": 255, "b": 215}},
        {"color_id": 231, "name": "Grey100", "rgb": {"r": 255, "g": 255, "b": 255}},
    ]
    grey_colors = [
        {"color_id": 7, "name": "Silver", "rgb": {"r": 192, "g": 192, "b": 192}},
        {"color_id": 232, "name": "Grey3", "rgb": {"r": 8, "g": 8, "b": 8}},
        {"color_id": 233, "name": "Grey7", "rgb": {"r": 18, "g": 18, "b": 18}},
        {"color_id": 234, "name": "Grey11", "rgb": {"r": 28, "g": 28, "b": 28}},
        {"color_id": 235, "name": "Grey15", "rgb": {"r": 38, "g": 38, "b": 38}},
        {"color_id": 236, "name": "Grey19", "rgb": {"r": 48, "g": 48, "b": 48}},
        {"color_id": 237, "name": "Grey23", "rgb": {"r": 58, "g": 58, "b": 58}},
        {"color_id": 238, "name": "Grey27", "rgb": {"r": 68, "g": 68, "b": 68}},
        {"color_id": 239, "name": "Grey30", "rgb": {"r": 78, "g": 78, "b": 78}},
        {"color_id": 240, "name": "Grey35", "rgb": {"r": 88, "g": 88, "b": 88}},
        {"color_id": 241, "name": "Grey39", "rgb": {"r": 98, "g": 98, "b": 98}},
        {"color_id": 242, "name": "Grey42", "rgb": {"r": 108, "g": 108, "b": 108}},
        {"color_id": 243, "name": "Grey46", "rgb": {"r": 118, "g": 118, "b": 118}},
        {"color_id": 244, "name": "Grey50", "rgb": {"r": 128, "g": 128, "b": 128}},
        {"color_id": 245, "name": "Grey54", "rgb": {"r": 138, "g": 138, "b": 138}},
        {"color_id": 246, "name": "Grey58", "rgb": {"r": 148, "g": 148, "b": 148}},
        {"color_id": 247, "name": "Grey62", "rgb": {"r": 158, "g": 158, "b": 158}},
        {"color_id": 248, "name": "Grey66", "rgb": {"r": 168, "g": 168, "b": 168}},
        {"color_id": 249, "name": "Grey70", "rgb": {"r": 178, "g": 178, "b": 178}},
        {"color_id": 250, "name": "Grey74", "rgb": {"r": 188, "g": 188, "b": 188}},
        {"color_id": 251, "name": "Grey78", "rgb": {"r": 198, "g": 198, "b": 198}},
        {"color_id": 252, "name": "Grey82", "rgb": {"r": 208, "g": 208, "b": 208}},
        {"color_id": 253, "name": "Grey85", "rgb": {"r": 218, "g": 218, "b": 218}},
        {"color_id": 254, "name": "Grey89", "rgb": {"r": 228, "g": 228, "b": 228}},
        {"color_id": 255, "name": "Grey93", "rgb": {"r": 238, "g": 238, "b": 238}},
    ]
    name_map = {}
    rgb_map = {}
    hex_map = {}
    groups = []
    for color in search_colors:
        if color["rgb"]["r"] not in groups:
            groups.append(color["rgb"]["r"])
        if color["name"].lower() not in name_map:
            name_map[color["name"].lower()] = color["color_id"]
        rgb = f'rgb({color["rgb"]["r"]},{color["rgb"]["g"]},{color["rgb"]["b"]})'
        if rgb not in rgb_map:
            rgb_map[rgb] = color["color_id"]
        hex_cd = "".join(
            [
                "#",
                format(color["rgb"]["r"], "02x"),
                format(color["rgb"]["g"], "02x"),
                format(color["rgb"]["b"], "02x"),
            ]
        ).lower()
        if hex_cd not in hex_map:
            hex_map[hex_cd] = color["color_id"]

    for color in grey_colors:
        if color["name"].lower() not in name_map:
            name_map[color["name"].lower()] = color["color_id"]
            rgb = f'rgb({color["rgb"]["r"]},{color["rgb"]["g"]},{color["rgb"]["b"]})'
            if rgb not in rgb_map:
                rgb_map[rgb] = color["color_id"]
            hex_cd = "".join(
                [
                    "#",
                    format(color["rgb"]["r"], "02x"),
                    format(color["rgb"]["g"], "02x"),
                    format(color["rgb"]["b"], "02x"),
                ]
            ).lower()
            if hex_cd not in hex_map:
                hex_map[hex_cd] = color["color_id"]

    best_rgb = {}
    best_hex = {}
    best_grey_rgb = {}
    best_grey_hex = {}
    for i in range(256):
        best_i = None
        for i_color in groups:
            if best_i is None or abs(i_color - i) < abs(best_i - i):
                best_i = i_color
        best_rgb[int(i)] = int(best_i)
        best_hex[format(i, "02x").lower()] = format(best_i, "02x").lower()

        for color in grey_colors:
            i_color = color["rgb"]["r"]
            if best_i is None or abs(i_color - i) < abs(best_i - i):
                best_i = i_color
        best_grey_rgb[int(i)] = int(best_i)
        best_grey_hex[format(i, "02x").lower()] = format(best_i, "02x").lower()

    name_map["default"] = DEFAULT_COLOR

    return {
        "name_map": name_map,
        "rgb_map": rgb_map,
        "hex_map": hex_map,
        "best_rgb": best_rgb,
        "best_hex": best_hex,
        "best_grey_rgb": best_grey_rgb,
        "best_grey_hex": best_grey_hex,
    }


def get_color_4_bit_lookup_dict() -> dict:
    """Return a 4-bit color code lookup dictionary.

    Returns
    -------
    dict
        The 4-bit color code lookup dictionary
    """
    search_colors = [
        {"color_id": 0, "name": "Black"},
        {"color_id": 1, "name": "Red"},
        {"color_id": 2, "name": "Green"},
        {"color_id": 3, "name": "Yellow"},
        {"color_id": 4, "name": "Blue"},
        {"color_id": 5, "name": "Magenta"},
        {"color_id": 6, "name": "Cyan"},
        {"color_id": 7, "name": "Light gray"},
        {"color_id": 60, "name": "Dark gray"},
        {"color_id": 61, "name": "Light red"},
        {"color_id": 62, "name": "Light green"},
        {"color_id": 63, "name": "Light yellow"},
        {"color_id": 64, "name": "Light blue"},
        {"color_id": 65, "name": "Light magenta"},
        {"color_id": 66, "name": "Light cyan"},
        {"color_id": 67, "name": "White"},
    ]
    name_map = {}
    for color in search_colors:
        if color["name"].lower() not in name_map:
            name_map[color["name"].lower()] = color["color_id"]

    name_map["default"] = DEFAULT_COLOR

    return {"name_map": name_map}


############
# Reset
############
def format_reset_text() -> str:
    """Return the reset escape code.

    Returns
    -------
    str
        The reset escape code
    """
    return "\\[\\033[0m\\]"


############
# System
############


def print_prompt(prompt_str: str):
    """Print the new prompt if it is valid, otherwise the existing prompt.

    Parameters
    ----------
    prompt_str: str
        The new prompt
    """
    if prompt_str:
        env_path = INSTALL_PATH / "prompt_functions.env"
        esc_prompt_str = prompt_str.replace('"', '\\"')
        print(f"source {env_path};")
        print(f'export PS1="{esc_prompt_str}"')
    else:
        print("# Keep existing prompt")


COLOR_4BIT_D = get_color_4_bit_lookup_dict()
COLOR_8BIT_D = get_color_8_bit_lookup_dict()

if __name__ == "__main__":
    main()
