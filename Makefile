format:
	black prompt_theme/prompt_theme.py

lint: format
	flake8 prompt_theme/prompt_theme.py
	pydocstyle prompt_theme/prompt_theme.py

install:
	python3 -m pip install --user .
